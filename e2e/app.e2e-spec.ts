import { AppPage } from './app.po';
import { browser, by, element } from 'protractor';

describe('angular-weather App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('app should render without crash', () => {
    expect(page);
  });
  it('app search input should allow to enter city name', () => {
    page.navigateTo();
    browser.waitForAngular();
    page.getInputSearch().sendKeys('London');
    expect(element(by.id('city')).getAttribute('value'))
      .toBe('London');
  });
  it('app search button click should populate the table', () => {
    page.navigateTo();
    browser.waitForAngular();
    page.getInputSearch().sendKeys('London');
    page.getBtnSearch().click().then(() => {
      browser.waitForAngular();
      expect(page.getTable().getInnerHtml).not.toBe(null);
    });
  });
});
