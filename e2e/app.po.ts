import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
  
  getInputSearch() {
    return element(by.id('city'));
  }
  getBtnSearch() {
    return element(by.id('btn-search'));
  }
  getTable() {
    return element(by.css('table'));
  }
}
