import { WeatherActionTypes, WeatherLoadError, WeatherLoadSuccess, WeatherSearch } from './weather';
import { testMock } from '../../test.help.spec';

describe('WeatherActions', () => {
  it('should create an action of type WeatherSearch', () => {
    const action = new WeatherSearch('London');
    expect(action.type).toEqual(WeatherActionTypes.WeatherSearch);
  });
  it('should create an action of type WeatherLoadSuccess', () => {
    const action = new WeatherLoadSuccess(testMock);
    expect(action.type).toEqual(WeatherActionTypes.WeatherLoadSuccess);
  });
  it('should create an action of type WeatherLoadError', () => {
    const action = new WeatherLoadError({ error: true });
    expect(action.type).toEqual(WeatherActionTypes.WeatherLoadError);
  });
});
