import { Action } from '@ngrx/store';
import { Weather } from '../../model/weather';

/**
 * Weather Action Types
 */
export enum WeatherActionTypes {
  WeatherSearch = '[Weather] WeatherSearch',
  WeatherLoadSuccess = '[Weather] WeatherLoadSuccess',
  WeatherLoadError = '[Weather] WeatherLoadError',
}

/**
 * Weather Action Union
 */
export type WeatherActionTypesUnion = WeatherSearch | WeatherLoadSuccess | WeatherLoadError;

/**
 * Search action
 */
export class WeatherSearch implements Action {
  readonly type = WeatherActionTypes.WeatherSearch;
  constructor(public payload: String) {
  }
}

/**
 * Load success
 */
export class WeatherLoadSuccess implements Action {
  readonly type = WeatherActionTypes.WeatherLoadSuccess;
  constructor(public payload: Weather) {
  }
}

/**
 * Load Error
 */
export class WeatherLoadError implements Action {
  readonly type = WeatherActionTypes.WeatherLoadError;
  constructor(public payload: any) {
  }
}
