import { async, TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { of } from 'rxjs/observable/of';
import { hot, cold } from 'jasmine-marbles';
import { WeatherEffects } from './weather';
import { WeatherService } from '../../weather/weather.service';
import { WeatherLoadError, WeatherLoadSuccess, WeatherSearch } from '../actions/weather';
import { testMock } from '../../test.help.spec';

describe('WeatherEffects - Marble', () => {
  let weatherEffects: WeatherEffects;
  let weatherService: jasmine.SpyObj<WeatherService>;
  let actions: any;
  beforeEach(async(() => TestBed.configureTestingModule({
      imports: [],
      providers: [
        WeatherEffects,
        provideMockActions(() => actions),
        {
          provide: WeatherService,
          useValue: jasmine.createSpyObj('WeatherService', ['searchWeatherForCity'])
        }
      ]
    })
  ));
  beforeEach(() => {
    weatherService = TestBed.get(WeatherService);
    weatherEffects = TestBed.get(WeatherEffects);
  });
  it('should loadWeather$ : success expect dispatch Action WeatherLoadSuccess', () => {
    weatherService.searchWeatherForCity.and.returnValue(of(testMock));
    const triggerAction: WeatherSearch = new WeatherSearch('London');
    const expectedAction: WeatherLoadSuccess = new WeatherLoadSuccess(testMock);
    actions = hot('--a-', { a: triggerAction });
    const expected = cold('--b', { b: expectedAction });
    expect(weatherEffects.loadWeather$).toBeObservable(expected);
  });
  it('should loadWeather$ : error expect dispatch Action WeatherLoadError', () => {
    weatherService.searchWeatherForCity.and.returnValue(ErrorObservable.create('error'));
    const triggerAction: WeatherSearch = new WeatherSearch('test');
    const expectedAction: WeatherLoadError = new WeatherLoadError('error');
    actions = hot('--a-', { a: triggerAction });
    const expected = cold('--b', { b: expectedAction });
    expect(weatherEffects.loadWeather$).toBeObservable(expected);
  });
});
