import { inject, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { WeatherEffects } from './weather';
import { WeatherService } from '../../weather/weather.service';
import { WeatherActionTypes, WeatherLoadError, WeatherLoadSuccess, WeatherSearch } from '../actions/weather';
import { EffectsRunner, EffectsTestingModule, MockWeatherService, testMock } from '../../test.help.spec';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

describe('WeatherEffects', () => {
  let runner, weatherEffects, weatherService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,
        EffectsTestingModule],
      providers: [WeatherEffects,
        {
          provide: WeatherService,
          useClass: MockWeatherService
        }]
    });
    runner = TestBed.get(EffectsRunner);
    weatherEffects = TestBed.get(WeatherEffects);
    weatherService = TestBed.get(WeatherService);
  });
  it('should be creating the service',
    inject([WeatherEffects], (service: WeatherEffects) => {
      expect(service).toBeTruthy();
    }));
  it('should have loadCompany$ @Effect method defined',
    inject([WeatherEffects], (service: WeatherEffects) => {
      expect(service.loadWeather$).toBeDefined();
    }));
  it('should loadWeather$ : success expect dispatch Action WeatherLoadSuccess', () => {
    spyOn(weatherEffects, 'loadWeather$').and.returnValue(new WeatherLoadSuccess(testMock));
    const result = new WeatherLoadSuccess(testMock);
    runner.queue(new WeatherSearch('London'));
    weatherEffects.loadWeather$.subscribe(data => {
      expect(data instanceof WeatherLoadSuccess).toEqual(true);
      expect(result instanceof WeatherLoadSuccess).toEqual(true);
    });
  });
  it('should loadWeather$ : error expect dispatch Action WeatherLoadError', () => {
    spyOn(weatherEffects, 'loadWeather$').and.returnValue(new WeatherLoadError('error'));
    spyOn(weatherService, 'searchWeatherForCity').and.returnValue(ErrorObservable.create('error'));
    runner.queue(new WeatherSearch('London'));
    weatherEffects.loadWeather$.subscribe(data => {
      expect(data.type).toEqual(WeatherActionTypes.WeatherLoadError);
    });
  });
});
