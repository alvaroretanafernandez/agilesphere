// TO BE IMPLEMENTED IF YOU DECIDE TO USE NG-RX
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { WeatherActionTypes, WeatherLoadError, WeatherLoadSuccess } from '../actions/weather';
import { catchError, switchMap, map } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { WeatherService } from '../../weather/weather.service';

@Injectable()
export class WeatherEffects {
  @Effect()
  loadWeather$: Observable<Action> = this.actions$.pipe(
    ofType(WeatherActionTypes.WeatherSearch),
     switchMap((action) =>
      this.weatherService.searchWeatherForCity(action['payload'])
        .pipe(
          map(weather => new WeatherLoadSuccess(weather)),
          catchError(error => of(new WeatherLoadError(error)))
        )
    )
  );
  constructor(private actions$: Actions,
              private weatherService: WeatherService) {
  }
}
