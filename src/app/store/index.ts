
import * as fromWeather from './reducers/weather';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

export interface WeatherState {
  weather: fromWeather.State;
}

export const WeatherReducers: ActionReducerMap<WeatherState> = {
  weather: fromWeather.reducer
};

export const getWeatherState = createFeatureSelector<WeatherState>('weather');
export const getAppWeatherState = createSelector(getWeatherState, state => state.weather);
export const getList = createSelector(getAppWeatherState, weather => weather ? weather.results : []);
