import * as fromWeather from '../reducers/weather';
import {  reducer } from '../reducers/weather';
import { State } from './weather';
import { WeatherActionTypes, WeatherLoadError, WeatherLoadSuccess, WeatherSearch } from '../actions/weather';
import { testMock } from '../../test.help.spec';

describe('WeatherReducer', () => {
  const testState: State = {
    loading: false,
    loaded: false,
    results: null
  };
  it('WeatherState : should have loading field defined', () => {
    expect(Object.keys(testState)[0]).toBe('loading');
  });
  it('WeatherState : should have loaded field defined', () => {
    expect(Object.keys(testState)[1]).toBe('loaded');
  });
  it('WeatherState : should have results field defined', () => {
    expect(Object.keys(testState)[2]).toBe('results');
  });
  it('undefined action : should return default state', () => {
    const { initialState } = fromWeather;
    const action = {} as any;
    const state = reducer(undefined, action);
    expect(state).toBe(initialState);
  });
  it('WeatherSearch action : should set loading to true', () => {
    const { initialState } = fromWeather;
    const action = new WeatherSearch('London');
    const state = reducer(initialState, action);
    expect(action.type).toBe(WeatherActionTypes.WeatherSearch);
    expect(state.loading).toBe(true);
    expect(state.loaded).toEqual(false);
  });
  it('WeatherLoadSuccess action : should populate results', () => {
    const { initialState } = fromWeather;
    const action = new WeatherLoadSuccess(testMock);
    const state = reducer(initialState, action);
    expect(state.loading).toBe(false);
    expect(state.loaded).toEqual(true);
    expect(state.results).not.toBe(null);
  });
  it('WeatherLoadError action : should set loaded false', () => {
    const { initialState } = fromWeather;
    const action = new WeatherLoadError({error: true});
    const state = reducer(initialState, action);
    expect(state.loading).toBe(false);
    expect(state.loaded).toEqual(false);
    expect(state.results).not.toBe(null);
  });
});
