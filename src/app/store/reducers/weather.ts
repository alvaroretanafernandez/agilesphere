import { WeatherActionTypes, WeatherActionTypesUnion } from '../actions/weather';


/**
 * Interface state
 */
export interface State {
  loaded: boolean;
  loading: boolean;
  results: any;
}

/**
 * default initial state
 */
export const initialState: State = {
  loading: false,
  loaded: false,
  results: []
};

/**
 * weather reducer
 * @param {State} state
 * @param {WeatherActionTypesUnion} action
 * @returns {State}
 */
export function reducer(state = initialState, action: WeatherActionTypesUnion): State {
  switch (action.type) {
    case WeatherActionTypes.WeatherSearch: {
      return {
        ...state,
        loading: true,
      };
    }
    case WeatherActionTypes.WeatherLoadSuccess: {
      return {
        ...state,
        loading: false,
        loaded: true,
        results: [
          ...state.results,
          action.payload
        ]
      };
    }
    case WeatherActionTypes.WeatherLoadError: {
      return {
        ...state,
      };
    }
    default: {
      return state;
    }
  }
}
