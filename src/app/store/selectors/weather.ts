import { State } from '../reducers/weather';
import { MemoizedSelector, createSelector } from '@ngrx/store';
import { WeatherState } from '../index';
import { Weather } from '../../model/weather';
export const selectWeather = (state: WeatherState) => state.weather;
export class WeatherSelectors {
  public static results: MemoizedSelector<WeatherState, Array<Weather>> = createSelector(selectWeather,
    (state: State) => state ? state.results : []
  );
}

