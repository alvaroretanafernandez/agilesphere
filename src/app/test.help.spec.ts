/**
 * NGRX test runner mock
 */
import { Injectable, NgModule } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Actions } from '@ngrx/effects';
import { Observable } from '../../node_modules/rxjs';


@Injectable()
export class EffectsRunner extends ReplaySubject<any> {
  constructor() {
    super();
  }
  queue(action: any) {
    this.next(action);
  }
}
export function _createActions(runner: EffectsRunner): Actions {
  return new Actions(runner);
}
@NgModule({
  providers: [
    EffectsRunner,
    { provide: Actions, deps: [EffectsRunner], useFactory: _createActions }
  ]
})
export class EffectsTestingModule {}
export const testMock = {
  'cod': '200',
  'message': 0.0123,
  'cnt': 8,
  'list': [{
    'dt': 1542402000,
    'main': {
      'temp': 7.69,
      'temp_min': 7.69,
      'temp_max': 9.12,
      'pressure': 1031.98,
      'sea_level': 1039.69,
      'grnd_level': 1031.98,
      'humidity': 92,
      'temp_kf': -1.43
    },
    'weather': [{ 'id': 500, 'main': 'Rain', 'description': 'light rain', 'icon': '10n' }],
    'clouds': { 'all': 56 },
    'wind': { 'speed': 2.97, 'deg': 100.002 },
    'rain': { '3h': 0.0024999999999999 },
    'sys': { 'pod': 'n' },
    'dt_txt': '2018-11-16 21:00:00'
  }, {
    'dt': 1542412800,
    'main': {
      'temp': 6.96,
      'temp_min': 6.96,
      'temp_max': 8.03,
      'pressure': 1032.48,
      'sea_level': 1040.19,
      'grnd_level': 1032.48,
      'humidity': 93,
      'temp_kf': -1.07
    },
    'weather': [{ 'id': 802, 'main': 'Clouds', 'description': 'scattered clouds', 'icon': '03n' }],
    'clouds': { 'all': 32 },
    'wind': { 'speed': 3.12, 'deg': 100 },
    'rain': {},
    'sys': { 'pod': 'n' },
    'dt_txt': '2018-11-17 00:00:00'
  }, {
    'dt': 1542423600,
    'main': {
      'temp': 6.59,
      'temp_min': 6.59,
      'temp_max': 7.3,
      'pressure': 1032.18,
      'sea_level': 1039.96,
      'grnd_level': 1032.18,
      'humidity': 92,
      'temp_kf': -0.71
    },
    'weather': [{ 'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '01n' }],
    'clouds': { 'all': 0 },
    'wind': { 'speed': 3.08, 'deg': 96.0121 },
    'rain': {},
    'sys': { 'pod': 'n' },
    'dt_txt': '2018-11-17 03:00:00'
  }, {
    'dt': 1542434400,
    'main': {
      'temp': 4.99,
      'temp_min': 4.99,
      'temp_max': 5.34,
      'pressure': 1032.2,
      'sea_level': 1040,
      'grnd_level': 1032.2,
      'humidity': 94,
      'temp_kf': -0.36
    },
    'weather': [{ 'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '01n' }],
    'clouds': { 'all': 0 },
    'wind': { 'speed': 3.01, 'deg': 106.5 },
    'rain': {},
    'sys': { 'pod': 'n' },
    'dt_txt': '2018-11-17 06:00:00'
  }, {
    'dt': 1542445200,
    'main': {
      'temp': 6.04,
      'temp_min': 6.04,
      'temp_max': 6.04,
      'pressure': 1033.02,
      'sea_level': 1040.86,
      'grnd_level': 1033.02,
      'humidity': 86,
      'temp_kf': 0
    },
    'weather': [{ 'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '01d' }],
    'clouds': { 'all': 0 },
    'wind': { 'speed': 3.87, 'deg': 106 },
    'rain': {},
    'sys': { 'pod': 'd' },
    'dt_txt': '2018-11-17 09:00:00'
  }, {
    'dt': 1542456000,
    'main': {
      'temp': 9.15,
      'temp_min': 9.15,
      'temp_max': 9.15,
      'pressure': 1033.43,
      'sea_level': 1041.11,
      'grnd_level': 1033.43,
      'humidity': 78,
      'temp_kf': 0
    },
    'weather': [{ 'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '01d' }],
    'clouds': { 'all': 0 },
    'wind': { 'speed': 4.91, 'deg': 106.003 },
    'rain': {},
    'sys': { 'pod': 'd' },
    'dt_txt': '2018-11-17 12:00:00'
  }, {
    'dt': 1542466800,
    'main': {
      'temp': 8.61,
      'temp_min': 8.61,
      'temp_max': 8.61,
      'pressure': 1033.25,
      'sea_level': 1040.96,
      'grnd_level': 1033.25,
      'humidity': 72,
      'temp_kf': 0
    },
    'weather': [{ 'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '01d' }],
    'clouds': { 'all': 0 },
    'wind': { 'speed': 5.37, 'deg': 97.0026 },
    'rain': {},
    'sys': { 'pod': 'd' },
    'dt_txt': '2018-11-17 15:00:00'
  }, {
    'dt': 1542477600,
    'main': {
      'temp': 7.2,
      'temp_min': 7.2,
      'temp_max': 7.2,
      'pressure': 1033.87,
      'sea_level': 1041.64,
      'grnd_level': 1033.87,
      'humidity': 73,
      'temp_kf': 0
    },
    'weather': [{ 'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '01n' }],
    'clouds': { 'all': 0 },
    'wind': { 'speed': 5.47, 'deg': 100.504 },
    'rain': {},
    'sys': { 'pod': 'n' },
    'dt_txt': '2018-11-17 18:00:00'
  }],
  'city': { 'id': 2643743, 'name': 'London', 'coord': { 'lat': 51.5073, 'lon': -0.1277 }, 'country': 'GB', 'population': 1000000 }
};
export class MockWeatherService {
  searchWeatherForCity(city): Observable<any> {
    return Observable.of(testMock);
  }
}
