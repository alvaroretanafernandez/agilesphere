import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange } from '@angular/core';
import { ResultsComponent } from './results.component';
import { Store } from '@ngrx/store';
import { testMock } from '../../../test.help.spec';

describe('ResultsComponent', () => {
  let component: ResultsComponent;
  let fixture: ComponentFixture<ResultsComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have headers attribute defined', () => {
    expect(component.headers).toBeTruthy();
  });
  it('should have @Input results attribute defined', () => {
    expect(component.results).toBe(undefined);
  });
  it('should component implement OnChanges', () => {
    expect(component.ngOnChanges).toBeTruthy();
  });
  it('should component react when results update with OnChanges',  () => {
    component.results = [testMock];
    const changes = new SimpleChange( null, [testMock], false);
    const element = fixture.nativeElement;
    component.ngOnChanges({results: changes});
    fixture.detectChanges();
    expect(element.querySelector('tbody')).toBeTruthy();
  });
});
