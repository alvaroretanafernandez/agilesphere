import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as moment from 'moment';
@Component({
  selector: 'app-results',
  templateUrl: './results.component.html'
})
export class ResultsComponent implements OnChanges {
  @Input() results;
  headers = [];
  ngOnChanges(changes: SimpleChanges) {
    if (changes.results.currentValue && changes.results.currentValue.length) {
      this.headers[0] = moment(this.results[0].list[0].dt_txt).format('h A');
      this.headers[1] = moment(this.results[0].list[2].dt_txt).format('h A');
      this.headers[2] = moment(this.results[0].list[4].dt_txt).format('h A');
      this.headers[3] = moment(this.results[0].list[6].dt_txt).format('h A');
    }
  }
}


