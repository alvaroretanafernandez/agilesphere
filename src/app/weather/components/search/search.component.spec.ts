import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SearchComponent } from './search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchComponent ],
      imports: [FormsModule, ReactiveFormsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have form attribute defined', () => {
    expect(component.form).toBeTruthy();
  });
  it('should have @Output onClickSearch attribute to emit event', () => {
    expect(component.onClickSearch).toBeTruthy();
  });
  it('should function defined to handle user click', () => {
    expect(component.search).toBeTruthy();
  });
  it('should emit event when user click', () => {
    spyOn(component.onClickSearch, 'emit');
    component.form.controls['city'].setValue('London');
    component.search();
    expect(component.onClickSearch.emit).toHaveBeenCalled();
  });
  it('should do nothing if form is not valid when user click', () => {
    spyOn(component.onClickSearch, 'emit');
    component.form.controls['city'].setValue('');
    component.search();
    expect(component.onClickSearch.emit).not.toHaveBeenCalled();
  });
});
