import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {
  /**
   * The form
   */
  form: FormGroup;
  /**
   * Output event on click
   * @type {EventEmitter<any>}
   */
  @Output() onClickSearch: EventEmitter<string> = new EventEmitter();

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'city': [ '', [Validators.required]],
    });
  }
  /**
   * Emit the evnet when user click
   */
  search() {
    if (this.form.invalid) {
      return;
    }
    this.onClickSearch.emit(this.form.controls['city'].value);
  }
}
