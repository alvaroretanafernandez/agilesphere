import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { WeatherContainerComponent } from './weather.container';
import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';
import { WeatherEffects } from '../store/effects/weather';
import { WeatherService } from './weather.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { WeatherSearch } from '../store/actions/weather';

describe('WeatherContainer', () => {
  let component: WeatherContainerComponent;
  let fixture: ComponentFixture<WeatherContainerComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherContainerComponent ],
      imports: [
        HttpClientTestingModule, StoreModule.forRoot({}),
        EffectsModule.forRoot([])],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [WeatherEffects, WeatherService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should have results$ attribute defined', () => {
    expect(component.results$).toBeTruthy();
  });
  it('should have search function defined', () => {
    expect(component.search).toBeTruthy();
  });
  it('should dispatch action when user click',  fakeAsync(() => {
    const mockStore = TestBed.get(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
    component.search('London');
    fixture.whenStable().then(() => {
       expect(mockStore.dispatch).toHaveBeenCalledWith(new WeatherSearch('London'));
    });
  }));
});
