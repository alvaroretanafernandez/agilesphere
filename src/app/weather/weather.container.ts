import { Component, OnInit } from '@angular/core';
import * as fromStore from '../store/index';
import { select, Store } from '@ngrx/store';
import { WeatherSearch } from '../store/actions/weather';
import { WeatherSelectors } from '../store/selectors/weather';
import { Observable } from 'rxjs/Observable';
import { Weather } from '../model/weather';

@Component({
  selector: 'app-weather',
  template: `
  <app-search (onClickSearch)="search($event)"></app-search>
  <app-results [results]="results$ | async"></app-results>  `
})
export class WeatherContainerComponent implements OnInit {
  results$: Observable<Weather[]>;
  constructor(public store: Store<fromStore.WeatherState>) {
  }
  search(val) {
    this.store.dispatch(new WeatherSearch(val));
  }
  public ngOnInit(): void {
    this.results$ = this.store.pipe(select(WeatherSelectors.results));
  }
}
