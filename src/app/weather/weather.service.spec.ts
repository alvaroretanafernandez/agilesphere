import { TestBed, inject, async } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { WeatherService } from './weather.service';

describe('WeatherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WeatherService]
    });
  });
  
  it('should be creating the service', inject([WeatherService], (service: WeatherService) => {
    expect(service).toBeTruthy();
  }));
  it('should have searchWeatherForCity method',
    inject([WeatherService], (service: WeatherService) => {
      expect(service.searchWeatherForCity).toBeDefined();
    }));
  it(' should have url attribute defined',
    inject([WeatherService], (service: WeatherService) => {
      expect(service.url).toBe('https://api.openweathermap.org/data/2.5/forecast');
    }));
  it(' should have params attribute defined',
    inject([WeatherService], (service: WeatherService) => {
      expect(service.params).toBeTruthy();
      expect(service.params.q).toBe('');
      expect(service.params.cnt).toBe('8');
      expect(service.params.units).toBe('metric');
      expect(service.params.APPID).toBe('010721642521f31b0fbc8c3831d45951');
    }));
  it('searchWeatherForCity should expect GET https://api.openweathermap.org/data/2.5/forecast',
    async(inject([WeatherService, HttpTestingController],
    (service: WeatherService, backend: HttpTestingController) => {
      service.searchWeatherForCity('London').subscribe();
      backend.expectOne(req => req.method === 'GET');
    })));
});
